﻿
/*
    Academic Integrity Declaration
    I, Aleksandra Bartosiak declare that except where I have
    referenced, the work I am are submitting in this attachment is my
    own work. I acknowledge and agree that the assessor of this assignment
    may, for the purpose of authenticating this assignment, reproduce it for the
    purpose of detecting plagiarism. I have read and am aware of the
    Think Education Policy and Procedure
    viewable online at
    think.edu.au/studying‐at‐think/policies‐and‐procedures

*/

#include <iostream>
#include <string>
#include "Point2d.h"
#include "textpixels_enums.h"
#include "textpixels.h"
#include <conio.h>
#include<fstream>
#include <fcntl.h>
#include <io.h>

using namespace std;
using namespace textpixels;


enum Screen
{
    MENU = 1,
    PLAY,
    PAUSE,
    GAMEOVER,
    EXIT,
    MODES,
    NONE
};

enum Modes
{
    HumanAgainstComputer = 1,
    HumanAgainstHuman ,
    CompAgainstComp,
    NON
};

const int LEVEL_WIDTH = 50;
const int LEVEL_HEIGHT = 20;
const int GUI_HEIGHT = 10;
int player = 1;
int modechoice = Modes::NON;

void drawLevelAndGui();
int displayMenuScreen();
void playTicTacToe();
void displayExitScreen();
int winner(string player);
string player1Name;
void drawX(int x, int y)
{
    // uses the central pixel of X as the arguments in the function
    drawPixel((x - 1), (y - 1), FG_DARK_GREEN);
    drawPixel((x + 1), (y - 1), FG_DARK_GREEN);
    drawPixel(x, y, FG_DARK_GREEN);
    drawPixel((x - 1), (y + 1), FG_DARK_GREEN);
    drawPixel((x + 1), (y + 1), FG_DARK_GREEN);
}
void drawO(int z, int m)
{
    drawPixel((z-1), (m-3), FG_DARK_YELLOW);
    drawPixel(z, (m-2), FG_DARK_YELLOW);
    drawPixel((z+1), (m-1), FG_DARK_YELLOW);
    drawPixel(z, m, FG_DARK_YELLOW); // 7,4
    drawPixel((z-1), (m+1), FG_DARK_YELLOW);
    drawPixel((z-2), m, FG_DARK_YELLOW);
    drawPixel((z-3), (m-1), FG_DARK_YELLOW);
    drawPixel((z-2), (m-2), FG_DARK_YELLOW);
}
void chooseDiffSpot()
{
    drawString(2, 10, "SPOT has been taken,", FG_DARK_YELLOW);
    drawString(2, 11, "choose a different one", FG_DARK_YELLOW);
    drawString(3, 13, "press any key to continue", FG_DARK_YELLOW);
    _getch();
}

int displayModes()
{
     do
     {
        textpixels::startFrame();
        fillWindow(FG_DARK_GREY);
        drawString(8, 8, "(1) 1 Player mode (against AI ;) ) ", layerColours(FG_MAGENTA, BG_GREY));
        drawString(8, 10, "(2) 2 Players mode", layerColours(FG_MAGENTA, BG_GREY));
        drawString(8, 12, "(3) AI against AI ", layerColours(FG_MAGENTA, BG_GREY));
        drawString(8, 17, "(M) Back to the Menu", layerColours(FG_MAGENTA, BG_GREY));

        if (keyIsDown('1'))
        {
            
            modechoice = 1;
            playTicTacToe();
        }
        else if (keyIsDown('2'))
        {
            modechoice = 2;
            playTicTacToe();
        }
        else if (keyIsDown('3'))
        {
            modechoice = 3;
            playTicTacToe();
        }
        else if (keyIsDown('M'))
        {
            displayMenuScreen();
            break;
        }
        textpixels::endFrame();

     } while (modechoice == Modes::NON);
    return(modechoice);
}

int main()
{
    textpixels::setupWindow(LEVEL_WIDTH, LEVEL_HEIGHT + GUI_HEIGHT, 22, 22);
    

    int screen = Screen::MENU;    

    /// Main game loop
    while (screen != Screen::EXIT)
    {
        if (screen == Screen::MENU)
        {
            Screen::MENU;
            screen = displayMenuScreen();
        }
        else if (screen == Screen::PLAY)
        {
            playTicTacToe();         
        }
        else if (screen == Screen::MODES)    // this one delete
            screen = displayModes();
        
    }
    if (screen == Screen::EXIT)
    {
        displayExitScreen();
    }
    return (0);
}

void drawLevelAndGui()
{
    fillWindow(FG_WHITE);
    fillRect(1, 1, LEVEL_WIDTH - 18, LEVEL_HEIGHT +4, FG_WHITE);
    fillRect(0, LEVEL_HEIGHT, LEVEL_WIDTH - 15, GUI_HEIGHT+5, FG_BLACK);
    fillRect(34, 0, 16, GUI_HEIGHT + LEVEL_HEIGHT, FG_BLACK);
    
   
    drawString(2, LEVEL_HEIGHT + 7, "(Q) ", FG_CYAN);
    drawString(6, LEVEL_HEIGHT + 7, "quit", FG_GREY);
    
    drawString(35, 4, "HOW TO PLAY ? ", FG_GREY);
    drawString(35, 6, "player 1 ", FG_GREY);
    if(modechoice == 2)
        drawString(35, 8, "player 2 ", FG_GREY);
    else if(modechoice == 1)
        drawString(35, 8, "COMPUTER ", FG_GREY);    // Human vs. computer
    else if (modechoice == 3) 
    {
        drawString(35, 6, "COMPUTER 1 ", FG_GREY);  // comp vs. comp
        drawString(35, 8, "COMPUTER 2 ", FG_GREY);
    }
    
    drawString(37, 11, " a | b | c ", FG_GREY);
    drawString(37, 12, "----------- ", FG_GREY);
    drawString(37, 13, " d | e | f ", FG_GREY);
    drawString(37, 14, "----------- ", FG_GREY);
    drawString(37, 15, " g | h | i ", FG_GREY);


    // tic tac toe lines 
    fillRectByCoords(11, 2, 11, 17, FG_DARK_GREY);
    fillRectByCoords(20, 2, 20, 17, FG_DARK_GREY);
    fillRectByCoords(4, 6, 27, 6, FG_DARK_GREY);
    fillRectByCoords(4, 13, 27, 13, FG_DARK_GREY);
   
    
}

void playTicTacToe()
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now(); // when x wins
    std::chrono::steady_clock::time_point begin2 = std::chrono::steady_clock::now();// when O wins
    std::chrono::steady_clock::time_point begin3 = std::chrono::steady_clock::now();// noone wins
    player = 1;  // repeated here because when we hit "U" - play again we want to start first , otherwise the computer starts 1st 
    bool collision = false;
    bool playerHasQuit = false;
    bool cross_won = false;
    bool circle_won = false;
    bool A_cross = false, B_cross = false, C_cross = false, D_cross = false, E_cross = false, F_cross = false, G_cross = false, H_cross = false, I_cross = false;
    bool A_circle = false, B_circle = false, C_circle = false, D_circle = false, E_circle = false, F_circle = false, G_circle = false, H_circle = false, I_circle = false, playAgain = false;
    int secondsCountdown = 0;
    string secondsString = to_string(secondsCountdown);
    
        do
        {

            textpixels::startFrame();
            
            if (keyIsDown('Q') && player ==1 || keyIsDown ('Q') && player == 2)
            {
                playerHasQuit = true;
                break;
            }
            
            // else the game swithes between player 1 and 2
            // PLAYER 1 STARTS BECAUSE player is set to 1 at the top
            drawLevelAndGui();
            
            //player 1 is, lets say us in both modes , stays the same  (unless CompAgainstComp mode chosen)
            //player 2 is either a computer or our friend we choose to play with (depending on mode)
            if (player == 1 && modechoice == 1||player == 1 && modechoice ==2)
            {
                
                drawPixel(46, 6, FG_MAGENTA); // pink pixel indicating whos turn it is 
                if (keyIsDown('A') && A_circle == false && A_cross == false)
                {
                    A_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('A') && A_circle == true || keyIsDown('A') && A_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('B') && B_circle == false && B_cross == false)
                {
                    B_cross = true;
                    B_circle = false;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('B') && B_circle == true || keyIsDown('B') && B_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('C') && C_circle == false && C_cross == false)
                {
                    C_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('C') && C_circle == true || keyIsDown('C') && C_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('D') && D_circle == false && D_cross == false)
                {
                    D_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('D') && D_circle == true || keyIsDown('D') && D_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('E') && E_circle == false && E_cross == false)
                {
                    E_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('E') && E_circle == true || keyIsDown('E') && E_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('F') && F_circle == false && F_cross == false)
                {
                    F_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('F') && F_circle == true || keyIsDown('F') && F_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('G') && G_circle == false && G_cross == false)
                {
                    G_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('G') && G_circle == true || keyIsDown('G') && G_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('H') && H_circle == false && H_cross == false)
                {
                    H_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('H') && H_circle == true || keyIsDown('H') && H_cross == true)
                    chooseDiffSpot();
                if (keyIsDown('I') && I_circle == false && I_cross == false)
                {
                    I_cross = true;
                    Sleep(1000);
                    player++;
                }
                else if (keyIsDown('I') && I_circle == true || keyIsDown('I') && I_cross == true)
                    chooseDiffSpot();
                
            }       
            
            // 2nd player = computer  - it is a simple blocking algorithm 
            else if (player == 2 && modechoice == 1)
            {
                drawPixel(46, 8, FG_DARK_MAGENTA);// pink pixel indicating whos turn it is 
                
                // The computer will block the winners combinations where last "X" is missing
                if (A_cross && B_cross&& !C_circle)
                
                {
                    C_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (A_cross && C_cross&&!B_circle)

                {
                    B_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (C_cross && B_cross&&!A_circle)

                {
                    A_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (D_cross && E_cross&&!F_circle)
                {
                    F_circle = true;
                    Sleep(1000);
                    player=1;
                }
               else if (D_cross && F_cross&& !E_circle)
                {
                    E_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (F_cross && E_cross&&!D_circle)
                {
                    D_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if(G_cross && H_cross && !I_circle)
                {
                    I_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (G_cross && I_cross&&!H_circle)
                {
                    H_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (I_cross && H_cross&&!G_circle)
                {
                    G_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if(A_cross && D_cross && !G_circle)
                {
                    G_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (A_cross && G_cross &&!D_circle)
                {
                    D_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (G_cross && D_cross && !A_circle)
                {
                    A_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (B_cross && E_cross && !H_circle)
                {
                    H_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (B_cross && H_cross && !E_circle)
                {
                    E_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (H_cross && E_cross && !B_circle)
                {
                    B_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if(C_cross && F_cross && !I_circle)
                {
                    I_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (C_cross && I_cross && !F_circle)
                {
                    F_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (F_cross && I_cross && !I_circle)
                {
                    I_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (A_cross && E_cross && !I_circle)
                {
                    I_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (A_cross && I_cross && !E_circle)
                {
                    E_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (I_cross && E_cross && !A_circle)
                {
                    A_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (G_cross && E_cross && !C_circle)
                {
                    C_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (G_cross && C_cross && !E_circle)
                {
                    E_circle = true;
                    Sleep(1000);
                    player=1;
                }
                else if (C_cross && E_cross && !G_circle)
                {
                    G_circle = true;
                    Sleep(1000);
                    player=1;
                }
                // if there is no "blocking opportunity" the computer might perform, then put a circle in a random place 
                // I'd love to use MINIMAX 
                else {
                char letters[] = { 'A','B','C','D','E','F','G','H','I' };
                char x = letters[rand() % 9];
                    if (x == 'A' && A_circle == false && A_cross == false)
                    {
                        A_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'A' && A_circle == true || x == 'A' && A_cross == true)
                        chooseDiffSpot();
                    if (x == 'B' && B_circle == false && B_cross == false)
                    {
                        B_circle = true;
                        B_cross = false;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'B' && B_circle == true || x == 'B' && B_cross == true)
                        chooseDiffSpot();
                    if (x == 'C' && C_circle == false && C_cross == false)
                    {
                        C_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'C' && C_circle == true || x == 'C' && C_cross == true)
                        chooseDiffSpot();
                    if (x == 'D' && D_circle == false && D_cross == false)
                    {
                        D_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'D' && D_circle == true || x == 'D' && D_cross == true)
                        chooseDiffSpot();
                    if (x == 'E' && E_circle == false && E_cross == false)
                    {
                        E_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if( x == 'E' && E_circle == true || x == 'E' && E_cross == true)
                        chooseDiffSpot();
                    if (x == 'F' && F_circle == false && F_cross == false)
                    {
                        F_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'F' && F_circle == true || x == 'F' && F_cross == true)
                        chooseDiffSpot();
                    if (x == 'G' && G_circle == false && G_cross == false)
                    {
                        G_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'G' && G_circle == true || x == 'G' && G_cross == true)
                        chooseDiffSpot();
                    if (x == 'H' && H_circle == false && H_cross == false)
                    {
                        H_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'H' && H_circle == true || x == 'H' && H_cross == true)
                        chooseDiffSpot();
                    if (x == 'I' && I_circle == false && I_cross == false)
                    {
                        I_circle = true;
                        Sleep(1000);
                        player = 1;
                    }
                    else if (x == 'I' && I_circle == true || x == 'I' && I_cross == true)
                        chooseDiffSpot();
                }
            }
            // 2nd player = human
            else if (player == 2 && modechoice == 2)
                        {
                            //textpixels::startFrame();
                            //drawLevelAndGui();
                            drawPixel(46, 8, FG_DARK_MAGENTA);// pink pixel indicating whos turn it is 
                            if (keyIsDown('A') && A_circle == false && A_cross == false)
                            {
                                A_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('A') && A_circle == true || keyIsDown('A') && A_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('B') && B_circle == false && B_cross == false)
                            {
                                B_circle = true;
                                B_cross = false;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('B') && B_circle == true || keyIsDown('B') && B_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('C') && C_circle == false && C_cross == false)
                            {
                                C_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('C') && C_circle == true || keyIsDown('C') && C_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('D') && D_circle == false && D_cross == false)
                            {
                                D_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('D') && D_circle == true || keyIsDown('D') && D_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('E') && E_circle == false && E_cross == false)
                            {
                                E_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('E') && E_circle == true || keyIsDown('E') && E_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('F') && F_circle == false && F_cross == false)
                            {
                                F_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('F') && F_circle == true || keyIsDown('F') && F_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('G') && G_circle == false && G_cross == false)
                            {
                                G_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('G') && G_circle == true || keyIsDown('G') && G_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('H') && H_circle == false && H_cross == false)
                            {
                                H_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('H') && H_circle == true || keyIsDown('H') && H_cross == true)
                                chooseDiffSpot();
                            if (keyIsDown('I') && I_circle == false && I_cross == false)
                            {
                                I_circle = true;
                                Sleep(1000);
                                player--;
                            }
                            else if (keyIsDown('I') && I_circle == true || keyIsDown('I') && I_cross == true)
                                chooseDiffSpot();
                        }
            // Computer against Computer 
            else if (player == 2 && modechoice == 3)
            {
            drawPixel(46, 8, FG_DARK_MAGENTA);// pink pixel indicating whos turn it is 

            // The computer will block the winners combinations where last "X" is missing
            if (A_cross && B_cross && !C_circle)

            {
                C_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (A_cross && C_cross && !B_circle)

            {
                B_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (C_cross && B_cross && !A_circle)

            {
                A_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (D_cross && E_cross && !F_circle)
            {
                F_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (D_cross && F_cross && !E_circle)
            {
                E_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (F_cross && E_cross && !D_circle)
            {
                D_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (G_cross && H_cross && !I_circle)
            {
                I_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (G_cross && I_cross && !H_circle)
            {
                H_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (I_cross && H_cross && !G_circle)
            {
                G_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (A_cross && D_cross && !G_circle)
            {
                G_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (A_cross && G_cross && !D_circle)
            {
                D_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (G_cross && D_cross && !A_circle)
            {
                A_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (B_cross && E_cross && !H_circle)
            {
                H_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (B_cross && H_cross && !E_circle)
            {
                E_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (H_cross && E_cross && !B_circle)
            {
                B_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (C_cross && F_cross && !I_circle)
            {
                I_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (C_cross && I_cross && !F_circle)
            {
                F_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (F_cross && I_cross && !I_circle)
            {
                I_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (A_cross && E_cross && !I_circle)
            {
                I_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (A_cross && I_cross && !E_circle)
            {
                E_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (I_cross && E_cross && !A_circle)
            {
                A_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (G_cross && E_cross && !C_circle)
            {
                C_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (G_cross && C_cross && !E_circle)
            {
                E_circle = true;
                Sleep(1000);
                player = 1;
            }
            else if (C_cross && E_cross && !G_circle)
            {
                G_circle = true;
                Sleep(1000);
                player = 1;
            }
            // if there is no "blocking opportunity" the computer might perform, then put a circle in a random place 
            // I'd love to use MINIMAX 
            else {
                char letters[] = { 'A','B','C','D','E','F','G','H','I' };
                char x = letters[rand() % 9];
                if (x == 'A' && A_circle == false && A_cross == false)
                {
                    A_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'A' && A_circle == true || x == 'A' && A_cross == true)
                    chooseDiffSpot();
                if (x == 'B' && B_circle == false && B_cross == false)
                {
                    B_circle = true;
                    B_cross = false;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'B' && B_circle == true || x == 'B' && B_cross == true)
                    chooseDiffSpot();
                if (x == 'C' && C_circle == false && C_cross == false)
                {
                    C_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'C' && C_circle == true || x == 'C' && C_cross == true)
                    chooseDiffSpot();
                if (x == 'D' && D_circle == false && D_cross == false)
                {
                    D_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'D' && D_circle == true || x == 'D' && D_cross == true)
                    chooseDiffSpot();
                if (x == 'E' && E_circle == false && E_cross == false)
                {
                    E_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'E' && E_circle == true || x == 'E' && E_cross == true)
                    chooseDiffSpot();
                if (x == 'F' && F_circle == false && F_cross == false)
                {
                    F_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'F' && F_circle == true || x == 'F' && F_cross == true)
                    chooseDiffSpot();
                if (x == 'G' && G_circle == false && G_cross == false)
                {
                    G_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'G' && G_circle == true || x == 'G' && G_cross == true)
                    chooseDiffSpot();
                if (x == 'H' && H_circle == false && H_cross == false)
                {
                    H_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'H' && H_circle == true || x == 'H' && H_cross == true)
                    chooseDiffSpot();
                if (x == 'I' && I_circle == false && I_cross == false)
                {
                    I_circle = true;
                    Sleep(1000);
                    player = 1;
                }
                else if (x == 'I' && I_circle == true || x == 'I' && I_cross == true)
                    chooseDiffSpot();
            }
            }
            else if (player == 1 && modechoice == 3)
            {
            drawPixel(46, 8, FG_DARK_MAGENTA);// pink pixel indicating whos turn it is 

            // The computer will block the winners combinations where last "X" is missing
            if (A_circle && B_circle && !C_cross)

            {
                C_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (A_circle && C_circle && !B_cross)

            {
                B_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (C_circle && B_circle && !A_cross)

            {
                A_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (D_circle && E_circle && !F_cross)
            {
                F_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (D_circle && F_circle && !E_cross)
            {
                E_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (F_circle && E_circle && !D_cross)
            {
                D_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (G_circle && H_circle && !I_cross)
            {
                I_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (G_circle && I_circle && !H_cross)
            {
                H_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (I_circle && H_circle && !G_cross)
            {
                G_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (A_circle && D_circle && !G_cross)
            {
                G_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (A_circle && G_circle && !D_cross)
            {
                D_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (G_circle && D_circle && !A_cross)
            {
                A_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (B_circle && E_circle && !H_cross)
            {
                H_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (B_circle && H_circle && !E_cross)
            {
                E_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (H_circle && E_circle && !B_cross)
            {
                B_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (C_circle && F_circle && !I_cross)
            {
                I_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (C_circle && I_circle && !F_cross)
            {
                F_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (F_circle && I_circle && !I_cross)
            {
                I_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (A_circle && E_circle && !I_cross)
            {
                I_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (A_circle && I_circle && !E_cross)
            {
                E_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (I_circle && E_circle && !A_cross)
            {
                A_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (G_circle && E_circle && !C_cross)
            {
                C_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (G_circle && C_circle && !E_cross)
            {
                E_cross = true;
                Sleep(1500);
                player = 2;
            }
            else if (C_circle && E_circle && !G_cross)
            {
                G_cross = true;
                Sleep(1500);
                player = 2;
            }
            // if there is no "blocking opportunity" the computer might perform, then put a circle in a random place 
            // I'd love to use MINIMAX 
            else {
                char letters[] = { 'A','B','C','D','E','F','G','H','I' };
                char x = letters[rand() % 9];
                if (x == 'A' && A_cross == false && A_circle == false)
                {
                    A_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'A' && A_cross == true || x == 'A' && A_circle == true)
                    chooseDiffSpot();
                if (x == 'B' && B_cross == false && B_circle == false)
                {
                    B_cross = true;
                    B_circle = false;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'B' && B_cross == true || x == 'B' && B_circle == true)
                    chooseDiffSpot();
                if (x == 'C' && C_cross == false && C_circle == false)
                {
                    C_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'C' && C_cross == true || x == 'C' && C_circle == true)
                    chooseDiffSpot();
                if (x == 'D' && D_cross == false && D_circle == false)
                {
                    D_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'D' && D_cross == true || x == 'D' && D_circle == true)
                    chooseDiffSpot();
                if (x == 'E' && E_cross == false && E_circle == false)
                {
                    E_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'E' && E_cross == true || x == 'E' && E_circle == true)
                    chooseDiffSpot();
                if (x == 'F' && F_cross == false && F_circle == false)
                {
                    F_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'F' && F_cross == true || x == 'F' && F_circle == true)
                    chooseDiffSpot();
                if (x == 'G' && G_cross == false && G_circle == false)
                {
                    G_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'G' && G_cross == true || x == 'G' && G_circle == true)
                    chooseDiffSpot();
                if (x == 'H' && H_cross == false && H_circle == false)
                {
                    H_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'H' && H_cross == true || x == 'H' && H_circle == true)
                    chooseDiffSpot();
                if (x == 'I' && I_cross == false && I_circle == false)
                {
                    I_cross = true;
                    Sleep(1500);
                    player = 2;
                }
                else if (x == 'I' && I_cross == true || x == 'I' && I_circle == true)
                    chooseDiffSpot();
            }
            }

            // CROSS IF STATEMENTS

            if (A_cross == true)
            {
                drawString(37, 11, "   ", FG_GREY);
                drawX(6, 3);
            }
            if (B_cross == true)
            {
                drawString(42, 11, "  ", FG_GREY);
                drawX(15, 3);
            }
            if (C_cross == true)
            {
                drawString(45, 11, "  ", FG_GREY);
                drawX(24, 3);
            }
            if (D_cross == true)
            {
                drawString(37, 13, "  ", FG_GREY);
                drawX(6, 10);
            }
            if (E_cross == true)
            {
                drawString(42, 13, "  ", FG_GREY);
                drawX(15, 10);
            }
            if (F_cross == true)
            {
                drawString(45, 13, "  ", FG_GREY);
                drawX(23, 10);
            }
            if (G_cross == true)
            {
                drawString(37, 15, "  ", FG_GREY);
                drawX(6, 16);
            }
            if (H_cross == true)
            {
                drawString(42, 15, "  ", FG_GREY);
                drawX(15, 16);
            }
            if (I_cross == true)
            {
                drawString(45, 15, "  ", FG_GREY);
                drawX(23, 16);
            }

            // CIRCLE IF STATEMENTS

            if (A_circle == true)
            {
                drawString(37, 11, "   ", FG_GREY);
                drawO(7, 4);

            }
            if (B_circle == true)
            {
                drawString(42, 11, "  ", FG_GREY);
                drawO(16, 4);
            }
            if (C_circle == true)
            {
                drawString(45, 11, "  ", FG_GREY);
                drawO(25, 4);
            }
            if (D_circle == true)
            {
                drawString(37, 13, "  ", FG_GREY);
                drawO(7, 11);
            }
            if (E_circle == true)
            {
                drawString(42, 13, "  ", FG_GREY);
                drawO(16, 11);
            }
            if (F_circle == true)
            {
                drawString(45, 13, "  ", FG_GREY);
                drawO(25, 11);
            }
            if (G_circle == true)
            {
                drawString(37, 15, "  ", FG_GREY);
                drawO(7, 17);
            }
            if (H_circle == true)
            {
                drawString(42, 15, "  ", FG_GREY);
                drawO(16, 17);
            }
            if (I_circle == true)
            {
                drawString(45, 15, "  ", FG_GREY);
                drawO(25, 17);
            }
            
            // WINNING CONDITIONS
           
            if (A_cross && B_cross && C_cross || D_cross && E_cross && F_cross || G_cross && H_cross && I_cross || A_cross && D_cross && G_cross || B_cross && E_cross && H_cross || C_cross && F_cross && I_cross || A_cross && E_cross && I_cross || G_cross && E_cross && C_cross)
            {
                
                //winner("Player X");
                std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                int x = ( std::chrono::duration_cast<std::chrono::seconds> (end - begin).count() );
                string xStr = to_string(x);
                drawString(25, LEVEL_HEIGHT + 4, xStr,FG_WHITE);
                drawString(11, LEVEL_HEIGHT + 4, "time elapsed: ", FG_BLUE);
                drawString(28, LEVEL_HEIGHT + 4, "seconds ", FG_BLUE);
                Sleep(1000);
                cross_won = true;
                winner("Player X");
                break;
            }
                
            
            if (A_circle && B_circle && C_circle || D_circle && E_circle && F_circle || G_circle && H_circle && I_circle || A_circle && D_circle && G_circle || B_circle && E_circle && H_circle || C_circle && F_circle && I_circle || A_circle && E_circle && I_circle || G_circle && E_circle && C_circle)
            {
                std::chrono::steady_clock::time_point end2 = std::chrono::steady_clock::now();
                int y = (std::chrono::duration_cast<std::chrono::seconds> (end2 - begin2).count());
                string yStr = to_string(y);
                drawString(25, LEVEL_HEIGHT + 4, yStr, FG_WHITE);
                drawString(11, LEVEL_HEIGHT + 4, "time elapsed: ", FG_BLUE);
                drawString(28, LEVEL_HEIGHT + 4, "seconds ", FG_BLUE);
                Sleep(1000);
                circle_won = true;
                winner("Player O");
                break;
            }

            // checks if all spots taken and nobody wins:
            else if ((A_circle||A_cross)&&(B_circle||B_cross)&&(C_circle||C_cross)&&(D_circle||D_cross)&&(E_circle||E_cross)&&(F_circle||F_cross)&&(G_circle||G_cross)&&(H_circle||H_cross)&&(I_circle||I_cross)&&!cross_won&&!circle_won)
            {
                std::chrono::steady_clock::time_point end3 = std::chrono::steady_clock::now();
                int y = (std::chrono::duration_cast<std::chrono::seconds> (end3 - begin3).count());
                string yStr = to_string(y);
                drawString(25, LEVEL_HEIGHT + 4, yStr, FG_WHITE);
                drawString(11, LEVEL_HEIGHT + 4, "time elapsed: ", FG_BLUE);
                drawString(28, LEVEL_HEIGHT + 4, "seconds ", FG_BLUE);
                Sleep(1000);
                winner("Nobody ");
                break;

            }
           textpixels::endFrame();
        } while (!playerHasQuit || cross_won == true || circle_won ==true);
    
        displayMenuScreen();
        return ;
}

int displayMenuScreen()
{
    int choice = Screen::NONE;
    do                            // Keeps looping, waiting for input
    {
        textpixels::startFrame();   // Needed always at start of game loop
        fillWindow(FG_DARK_GREY);
        drawString(12, 3, "(P) Play Tic-Tac-Toe", layerColours(FG_MAGENTA, BG_GREY));   //default easy unless we change the mode
        drawString(12, 6, "(E) Exit", layerColours(FG_MAGENTA, BG_GREY));
       
       
         if (keyIsDown('E'))
        {
            choice = Screen::EXIT;
            displayExitScreen();
        }
         if (keyIsDown('P') )
        {
            choice = Screen::MODES;
            displayModes();
        }
        textpixels::endFrame();
    } while (choice == Screen::NONE);     // Only stop when playerHasQuit  
    return(choice);
}

int winner(string player)
{
    int choiceW = Screen::NONE;

    do {
        textpixels::startFrame();
        drawString(11, LEVEL_HEIGHT + 1, (player + " is a winner !"), FG_GREEN);
        drawString(11, LEVEL_HEIGHT + 2, "press U to play again ");
        drawString(11, LEVEL_HEIGHT + 3, "press M for menu ");
        

        if (keyIsDown('U'))
             playTicTacToe();
            

        else if (keyIsDown('M'))
        {
            choiceW = displayMenuScreen();   // comes back to the menu ... but "P" does not swith further
            
        }
        textpixels::endFrame();
    }while (choiceW == Screen::NONE); 
    
    return(choiceW);
    
}

void displayExitScreen()
{
    textpixels::startFrame();
    fillWindow(FG_DARK_GREY);
    fillRectByCoords(19, 15, 26, 22, FG_DARK_GREY);
    drawString(11, 10, "Thank you for playing !!!!!!!!!", layerColours(FG_DARK_MAGENTA, BG_DARK_GREY));
    drawString(11, 12, "CREDITS:", layerColours(FG_DARK_MAGENTA, BG_DARK_GREY));
    drawString(11, 13, "Aleksandra Bartosiak ", layerColours(FG_DARK_MAGENTA, BG_DARK_GREY));
    drawPixel(20, 16, FG_MAGENTA);
    drawPixel(24, 16, FG_MAGENTA);
    drawPixel(20, 19, FG_MAGENTA);
    drawPixel(21, 20, FG_MAGENTA);
    drawPixel(22, 21, FG_MAGENTA);
    drawPixel(23, 21, FG_MAGENTA);
    drawPixel(24, 20, FG_MAGENTA);
    drawPixel(25, 19, FG_MAGENTA);
    return;
}


